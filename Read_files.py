from __future__ import division
import nibabel as nib
import numpy as np
import nibabel as nib
import os

# Define Train data and mask
def prepare_nifti(filename, slices ):
    Test_img = []
    #filename = r'C:\LUNG SEGMENTATION\IMG_0002.nii.gz'
    separator ='\\'
    folder = separator.join(filename.split(separator)[:-1])
    #slices = range(50,52)

    #for idx in range(len(Tr_list)):
    #    filename = Tr_list[idx]
    prefix = filename[len(folder) + 1:len(folder) + 4]
    if prefix == 'IMG':
        prefix = filename[len(folder) + 5:len(filename)]
        vol = nib.load(filename)
        # Get the axial images and corresponding masks
        vol_ims = hu_to_grayscale(vol.get_data())
        # Insert samples to the Train data, which has the segmentation label
        for idx in slices:
            if ~(np.sum(np.sum(np.sum(vol_ims[idx, :, :]))) == 0):
                Test_img.append(vol_ims[idx, :, :])


    Data_train = np.array(Test_img)
    out_folder = folder + '/processed_data/'
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    np.save(out_folder + 'data_test', Test_img)



def read_nifti(filename):
    vol = nib.load(filename)
    vol_ims = hu_to_grayscale(vol.get_data())
    return vol_ims



# Functions
def hu_to_grayscale(volume):
    volume = np.clip(volume, -512, 512)
    mxval  = np.max(volume)
    mnval  = np.min(volume)
    im_volume = (volume - mnval)/max(mxval - mnval, 1e-3)
    im_volume = im_volume
    return im_volume *255
